/*
Conexiones:
NodeMCU    -> Matrix
MOSI-D7-GPIO13  -> DIN
CLK-D5-GPIO14   -> Clk
GPIO0-D3        -> LOAD
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>
#include <ArduinoJson.h>

// =======================================================================
// Configuración del dispositivo:
// =======================================================================
const char* ssid     = "XXXXX";                      // Nombre de tu wifi
const char* password = "XXXXX";                    // contraseña de tu wifi
String weatherKey = "XXXXXX";  // API de OpenWeather. Crea una en http://openweathermap.org/api
String weatherLang = "&lang=es"; // Idioma de salida de OpenWeather
String cityID = "2511174"; //Santa Cruz de Tenerife (ID de tu ciudad). Busca la tuya en https://openweathermap.org/city
// =======================================================================


WiFiClient client;

String weatherMain = "";
String weatherDescription = "";
String weatherLocation = "";
String country;
int humidity;
int pressure;
float temp;
float tempMin, tempMax;
int clouds;
float windSpeed;
String date;
String currencyRates;
String weatherString;
String M_arr[13] = {" ", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"}; // Tradución meses a español
String Dow_arr[8] = {" ", "Lunes", "Martes", "Mi" + String (char(130)) + "rcoles", "Jueves", "Viernes", "S" + String (char(160)) + "bado", "Domingo"}; // Traducción días con tildes a español
long period;
int offset=1,refresh=0;
int pinCS = 0; // Conexión PIN CS
int numberOfHorizontalDisplays = 4; // Número de matrices LED horizontales
int numberOfVerticalDisplays = 1; // Número de matrices LED verticales
String decodedMsg;
Max72xxPanel matrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);

int wait = 50; // velocidad del texto

int spacer = 2;// Espacio entre pixel
int width = 5 + spacer; // Ancho de letra de 5 pixel

void setup(void) {

matrix.setIntensity(13); // Ajusta brillo de 0 a 15


// Coordenadas de origen de matrices 8*8
  matrix.setRotation(0, 1);        // Matriz 1
  matrix.setRotation(1, 1);        // Matriz 2 
  matrix.setRotation(2, 1);        // Matriz 3 
  matrix.setRotation(3, 1);        // Matriz 4 


  Serial.begin(115200);                           // Conexión Monitor Serie
  WiFi.mode(WIFI_STA);                           
  WiFi.begin(ssid, password);                         // Conexión WIFI
  while (WiFi.status() != WL_CONNECTED) {         // Si hay conexión, parpadea el led azul
    delay(500);
    Serial.print(".");
  }



}

// =======================================================================
#define MAX_DIGITS 16
byte dig[MAX_DIGITS]={0};
byte digold[MAX_DIGITS]={0};
byte digtrans[MAX_DIGITS]={0};
int updCnt = 0;
int dots = 0;
long dotTime = 0;
long clkTime = 0;
int dx=0;
int dy=0;
byte del=0;
int h,m,s;
float utcOffset = 0; //Tu zona horaria
long localEpoc = 0;
long localMillisAtUpdate = 0;
int day, month, year, dayOfWeek;
int monthnum = 0;
int downum = 0;
int summerTime = 0;
String dayloc; 
String monthloc; 
// =======================================================================
void loop(void) {




if(updCnt<=0) { // Cada 10 ciclos obtenemos datos de tiempo y clima
    updCnt = 10;
    Serial.println("Recibiendo datos ...");
    getWeatherData();
    getTime();
    Serial.println("Datos recibidos");
    clkTime = millis();
  }

  if(millis()-clkTime > 15000 && !del && dots) { //Cada 15 segundos comenzamos con fecha, clima y texto
ScrollText(date);//Fecha
ScrollText(weatherString);//Tiempo meteorológico
ScrollText("Reloj Podcast Linux"); //Aquí está el texto, cambia lo que desees o coméntalo para que no salga.
    updCnt--;
    clkTime = millis();
  }
  
  DisplayTime();
  if(millis()-dotTime > 500) {
    dotTime = millis();
    dots = !dots;
  }


}

// =======================================================================
void DisplayTime(){
    updateTime();
    matrix.fillScreen(LOW);
    int y = (matrix.height() - 8) / 2; // Centrar el texto verticalmente

    
    if(s & 1){matrix.drawChar(14, y, (String(":"))[0], HIGH, LOW, 1);} // Cada segundo imprimimos dos puntos en el centro (para parpadear)
    else{matrix.drawChar(14, y, (String(" "))[0], HIGH, LOW, 1);}
    
    String hour1 = String (h/10);
    String hour2 = String (h%10);
    String min1 = String (m/10);
    String min2 = String (m%10);
    String sec1 = String (s/10);
    String sec2 = String (s%10);
    int xh = 2;
    int xm = 19;
//    int xs = 28;

    matrix.drawChar(xh, y, hour1[0], HIGH, LOW, 1);
    matrix.drawChar(xh+6, y, hour2[0], HIGH, LOW, 1);
    matrix.drawChar(xm, y, min1[0], HIGH, LOW, 1);
    matrix.drawChar(xm+6, y, min2[0], HIGH, LOW, 1);
//    matrix.drawChar(xs, y, sec1[0], HIGH, LOW, 1);
//    matrix.drawChar(xs+6, y, sec2[0], HIGH, LOW, 1);  


  
    matrix.write(); // Imprimir en Pantalla
}

// =======================================================================
void DisplayText(String text){
    matrix.fillScreen(LOW);
    for (int i=0; i<text.length(); i++){
    
    int letter =(matrix.width())- i * (width-1);
    int x = (matrix.width() +1) -letter;
    int y = (matrix.height() - 8) / 2; // Centrar el texto verticalmente
    matrix.drawChar(x, y, text[i], HIGH, LOW, 1);
    matrix.write(); // Imprimir en Pantalla
    
    }

}
// =======================================================================
void ScrollText (String text){
    for ( int i = 0 ; i < width * text.length() + matrix.width() - 1 - spacer; i++ ) {
    if (refresh==1) i=0;
    refresh=0;
    matrix.fillScreen(LOW);
    int letter = i / width;
    int x = (matrix.width() - 1) - i % width;
    int y = (matrix.height() - 8) / 2; // Centrar el texto verticalmente
 
    while ( x + width - spacer >= 0 && letter >= 0 ) {
      if ( letter < text.length() ) {
        matrix.drawChar(x, y, text[letter], HIGH, LOW, 1);
      }
      letter--;
      x -= width;
    }
    matrix.write(); // Imprimir en Pantalla
    delay(wait);
  }
}


// =======================================================================
// RECIBIR DATOS DE OPENWEATHERMAP.ORG
// =======================================================================

const char *weatherHost = "api.openweathermap.org";

void getWeatherData()
{
  Serial.print("Connectando a "); Serial.println(weatherHost);
  if (client.connect(weatherHost, 80)) {
    client.println(String("GET /data/2.5/weather?id=") + cityID + "&units=metric&appid=" + weatherKey + weatherLang + "\r\n" +
                "Host: " + weatherHost + "\r\nUser-Agent: ArduinoWiFi/1.1\r\n" +
                "Connection: close\r\n\r\n");
  } else {
    Serial.println("connection failed");
    return;
  }
  String line;
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    Serial.println("w.");
    repeatCounter++;
  }
  while (client.available()) {
    char c = client.read(); 
    if (c == '[' || c == ']') c = ' ';
    line += c;
  }

  client.stop();

  DynamicJsonBuffer jsonBuf;
  JsonObject &root = jsonBuf.parseObject(line);
  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return;
  }
  //weatherMain = root["weather"]["main"].as<String>();
  weatherDescription = root["weather"]["description"].as<String>();
  weatherDescription.toLowerCase();
  //  weatherLocation = root["name"].as<String>();// Añade todos los datos que desees (Lluvia, nubosidad...). Están en https://openweathermap.org/current
  //  country = root["sys"]["country"].as<String>();
  temp = root["main"]["temp"];
  humidity = root["main"]["humidity"];
  pressure = root["main"]["pressure"];
  tempMin = root["main"]["temp_min"];
  tempMax = root["main"]["temp_max"];
  windSpeed = root["wind"]["speed"];
  clouds = root["clouds"]["all"];
  String deg = String(char('~'+25));
  weatherString = "T:" + String(temp,1)+String(char(247)) + "C ";
  weatherString += "Humedad:" + String(humidity) + "% ";
  weatherString += "Pres. at.:" + String(pressure) + "hpa ";
  weatherString += "Viento:" + String(windSpeed*3.6,1) + " km/h ";
  weatherString += "Cielo:" + weatherDescription;

}

// =======================================================================
// DESCARGAR FECHA Y HORA DE GOOGLE
// =======================================================================

void getTime()
{
  WiFiClient client;
  if (!client.connect("www.google.com", 80)) {
    Serial.println("connection to google failed");
    return;
  }

  client.print(String("GET / HTTP/1.1\r\n") +
               String("Host: www.google.com\r\n") +
               String("Connection: close\r\n\r\n"));
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    //Serial.println(".");
    repeatCounter++;
  }

  String line;
  client.setNoDelay(false);
  while(client.connected() && client.available()) {
    line = client.readStringUntil('\n');
    line.toUpperCase();
    if (line.startsWith("DATE: ")) {
      date = line.substring(6, 22);
      date.toUpperCase();
      Serial.println(date); //check
          
      int day = atoi (date.substring(5,7).c_str()); //Extrae el día
      int year = 2000 + atoi (date.substring(13,16).c_str()); //Extrae el año
      monthnum = month2index(date.substring(8,11)); // Corvierte el mes en número
      month = monthnum; // Los meses a número
      downum = dow2index(date.substring(0,3)); // Corvierte el día en número
      dayOfWeek = downum; // Los días de la semana a número
      monthloc = M_arr[month]; // localize month
      dayloc = Dow_arr[dayOfWeek]; // localize DayOfWeek


      //date = dayloc + ", " + day + " de " + monthloc + " de " + year; //Fecha extensa con nombres de los meses
      date = dayloc + ", " + day + "/" + month + "/" + year; // Fecha corta con números de los meses
      //date = day + "/" + month + "/" + year; // Fecha corta sin día de la semana

      h = line.substring(23, 25).toInt();
      m = line.substring(26, 28).toInt();
      s = line.substring(29, 31).toInt();
      localMillisAtUpdate = millis();
      localEpoc = (h * 60 * 60 + m * 60 + s);
    }
  }
  client.stop();
}


// =======================================================================

//Convertir la palabra del mes en número

int month2index (String month) 
{
        String months[12] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        for (int i = 0; i < 12; i++) {
                if (months[i] == month)
                        return i + 1;  
                        
        }
        return 0;
}

// =======================================================================

//Convertir la palabra del día en número

int dow2index (String dayOfWeek) 
{
        String dows[7] = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
        for (int i = 0; i < 12; i++) {
                if (dows[i] == dayOfWeek)
                        return i + 1;  
                        
        }
        return 0;
}

// =======================================================================

//Comprobar si estamos en horario de verano

int checkSummerTime()
{
  if(month>3 && month<10) return 1;
  if(month==3 && day>=31-(((5*year/4)+4)%7) ) return 1;
  if(month==10 && day<31-(((5*year/4)+1)%7) ) return 0;
  return 0;
  
}

// =======================================================================

void updateTime()
{
  long curEpoch = localEpoc + ((millis() - localMillisAtUpdate) / 1000);
  long epoch = lroundf(curEpoch + 3600 * (utcOffset+summerTime) + 86400L) % 86400L;
  h = ((epoch  % 86400L) / 3600) % 24;
  m = (epoch % 3600) / 60;
  s = epoch % 60;
}
